# 2019-10-04 No version yet. ##
----

- support for rectangular adaptive domain in 2D and 3D;
- support for hydrodynamical (HYDRO) and magnetohydrodynamical (MHD) equations, both in classical and relativistic formulations;
- support for adiabatic (ADI) and isothermal (ISO) equation of state;
- support for viscosity and resistivity source terms;
- support for passive scalars;
- time integration using Euler and 2nd order Runge-Kutta methods or up to 4th order Stron Stability Preserving Runge-Kutta;
- a number of spatial interpolation using 2nd order TVD methods, up to 9th order Monotonicity-Preserving;
- HLL-family of approximate Riemann solvers (HLL, HLLC, and HLLD);
- GLM scheme for the induction equation;
- MPI parallelization;